
% estimate MCM meeting time given the separation between ORC binding sites
% first loaded MCM can hinder the binding of 2nd ORC and thus loading of
% 2nd MCM complex.


%% Code to determine meeting times of two 1D diffusers w.r.t separation between them

clc
% close all 
clear

%%
% Define parameters
McmSH_diffusion_const = 800; % bp^2/s

origin_separation = [100]; % bp

% decay parameters: typical dwell time of McmSH on DNA
McmSH_half_life_minutes = 3;

McmSH_half_life_seconds = McmSH_half_life_minutes.*60;
McmSH_decay_time = -McmSH_half_life_seconds./log(0.5); % convert hafl-life to exponential decay time

% basepairs occupied on DNA
MCMSH_bp = 0;
ORC_bp = 0; % ORC overhang after 5' of ORC binding site


% setup parameters
num_runs = 1; % total interations

dt = 0.001; % seconds
sigma = sqrt(McmSH_diffusion_const*2*dt)
cutoff = 1.2E6; % max iters to simulate a trajectory
max_run_time = dt*cutoff/60 % minutes
not_met_trajs = zeros(size(origin_separation));

num_hist_bins = 20;
cumulative_Y = zeros(length(McmSH_decay_time),num_hist_bins);

t = logspace(-4, 4, num_hist_bins);
t= [0,t];

%% simulation
tic

for M = 1:length(origin_separation) 
    
    intersection_time = zeros(num_runs,1); % iteratrion index of meeting
    not_intersected_counts = 0; % trajectories that did not meet within cutoff
    decay_time = McmSH_decay_time; % decay time constant
    separation = origin_separation(M); % initial position
    MCM_SH2_load_time = zeros(num_runs,1);% iteration index of mcmsh2 loading

    for n = 1:num_runs
        MCM_SH1_pos = zeros(cutoff,1);
        MCM_SH2_pos = zeros(cutoff,1);
        
        ORI1_location = 0; % inside end of ORI1
        MCM_SH1_pos(1) = ORI1_location + ORC_bp + MCMSH_bp; % initial position of N-terminal of MCMSH1
        
        ORI2_location = ORI1_location + separation ; % inside edge of ORI2 

        decay_time1 = exprnd(decay_time);
        decay_time2 = exprnd(decay_time);
        decay_iter1 = min(decay_time1,decay_time2)/dt;
    
        % diffuse mcm1 till mcm2 can be loaded
        MCM_SH2_loaded = false;
        i = 1;
        while MCM_SH2_loaded == false
            % check if 2nd MCM can be loaded
            if ORI2_location-MCM_SH1_pos(i) > (ORC_bp+MCMSH_bp) % load mcmsh2
                MCM_SH2_pos(i) = ORI2_location-(ORC_bp+MCMSH_bp);
                MCM_SH2_loaded = true;
                MCM_SH2_load_time(n) = i;
                decay_iter2 = i+(decay_time2/dt);
            else 
                MCM_SH2_pos(i) = NaN;
                MCM_SH2_loaded = false;
            end
            i = i+1;
            MCM_SH1_pos(i) = MCM_SH1_pos(i-1) + sigma*randn(1);
            if i>min(cutoff, decay_time1/dt )
                intersection_time(n) = Inf;
                not_intersected_counts = not_intersected_counts + 1;
                % 'cutoff/decay mcm1'
                MCM_SH1_decayed = true;
                break
            else 
                MCM_SH1_decayed = false;
            end
        end
       
        % MCM2 is loaded and MCM1 is still present
        % continue the interation to check if two MCMs meet
        
        if MCM_SH1_decayed==false
            MEET=0;
            while MEET ==0
                MCM_SH1_pos(i) = MCM_SH1_pos(i-1) + sigma*randn(1);
                MCM_SH2_pos(i) = MCM_SH2_pos(i-1) + sigma*randn(1);
                if abs(MCM_SH1_pos(i)-MCM_SH2_pos(i)) < 1*sigma
                    intersection_time(n) = i*dt;
                    MEET = 1;
                    % 'meet'
                    break
                end
    
                if i>min(cutoff, min(decay_iter2,decay_iter1) )
                    intersection_time(n) = Inf;
                    not_intersected_counts = not_intersected_counts + 1;
                    % 'cutoff/decay both'
                    break
                end
                i = i+1;
            end
        end
    end
    
    % remove Inf intersection times
    intersection_time = intersection_time(intersection_time~=Inf);
    
    mean_hitting_times = mean(intersection_time(intersection_time~=Inf))
    not_met_trajs(M) = not_intersected_counts

    for ii = 1:length(t)-1
        cumulative_Y(M,ii) = sum(t(ii)<intersection_time & intersection_time<t(ii+1))/num_runs;
    end

end
toc
t(1) = [];


%% plotting 

figure(1)
subplot(2,2,1)
mcm1_trace = MCM_SH1_pos(MCM_SH1_pos ~= 0);
mcm2_trace = MCM_SH2_pos(MCM_SH1_pos ~= 0);

tmax = length(mcm2_trace);
time = dt:dt:tmax*dt;

plot(time,mcm1_trace,'LineWidth',2)
hold on 
plot(time,mcm2_trace,'LineWidth',2)
hold off
xlabel('time (s)')
ylabel('MCM N-termini position (bp)')
title('Trajectories of two diffusing MCM-SH')
legend('MCM-SH1','MCM-SH2')
set (gca, 'XGrid','on','YGrid','on','FontSize',15)