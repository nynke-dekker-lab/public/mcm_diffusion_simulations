Simulations to predict MCMDH yield as a function of separation of origins of replication sites onto the DNA

Written and Tested using MATLAB 2023a in Windows 10 and MacOS

Requirements
-MATLAB parallel computing toolbox

Setup and installation
- The codes are readily executable

Typical time to run the codes
- 120 seconds (Macbook pro M2, 10 cores CPU)
